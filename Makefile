DIR = $(lastword $(subst /, ,$(abspath $(dir $(lastword $(MAKEFILE_LIST))))))

include .env

PIHOLE_TAG ?= latest
UNBOUND_TAG ?= latest
DHCP_HELPER_TAG ?= latest
PLATFORM = linux/arm/v7,linux/arm64/v8,linux/amd64
SERVICES = pihole unbound dhcp-helper
SERVICES_PAT = ($(subst $(eval) ,|,$(SERVICES)))
PIHOLE_DEPS = $(addprefix ./pihole/, Dockerfile start)
UNBOUND_DEPS = $(addprefix ./unbound/, Dockerfile start pi-hole.conf)
DHCP_HELPER_DEPS = ./dhcp-helper/Dockerfile
BUILD = docker buildx build --push --platform $(PLATFORM) --tag
TEST = DOCKER_BUILDKIT=1 docker build -t
BUILDER_INFO = $(shell docker buildx ls | grep -m 1 \*)
BUILDER_TYPE = $(word 3, $(BUILDER_INFO))
BUILDER_NAME = $(word 1, $(BUILDER_INFO))
BUILDER = docker buildx create --use
UP = docker-compose up -d
DOWN = docker-compose down
EXEC = docker exec -it
LOGS = docker logs
RMI = docker image rm

.PHONY: all builder test up down execp execu execd logp logu logd clean

all: $(addprefix .make/, pihole unbound dhcp-helper)

.make/pihole: $(PIHOLE_DEPS) | .make
	$(BUILD) narvin/pihole:$(PIHOLE_TAG) ./pihole
ifneq ($(PIHOLE_TAG), latest)
	$(BUILD) narvin/pihole:latest ./pihole
endif
	touch $@

.make/unbound: $(UNBOUND_DEPS) | .make
	$(BUILD) narvin/unbound:$(UNBOUND_TAG) ./unbound
ifneq ($(UNBOUND_TAG), latest)
	$(BUILD) narvin/unbound:latest ./unbound
endif
	touch $@

.make/dhcp-helper: $(DHCP_HELPER_DEPS) | .make
	$(BUILD) narvin/dhcp-helper:$(DHCP_HELPER_TAG) ./dhcp-helper
ifneq ($(DHCP_HELPER_TAG), latest)
	$(BUILD) narvin/dhcp-helper:latest ./dhcp-helper
endif
	touch $@

test: $(addprefix .make/, $(addsuffix -test, $(SERVICES)))

.make/pihole-test: $(PIHOLE_DEPS) | .make
	$(TEST) narvin/pihole:$(PIHOLE_TAG) ./pihole
	touch $@

.make/unbound-test: $(UNBOUND_DEPS) | .make
	$(TEST) narvin/unbound:$(UNBOUND_TAG) ./unbound
	touch $@

.make/dhcp-helper-test: $(DHCP_HELPER_DEPS) | .make
	$(TEST) narvin/dhcp-helper:$(DHCP_HELPER_TAG) ./dhcp-helper
	touch $@

builder:
ifneq ($(BUILDER_TYPE), docker-container)
	$(BUILDER)
else
	$(info Using builder $(BUILDER_NAME))
endif

up:
ifneq ($(shell docker ps | grep -cE '$(DIR)_$(SERVICES_PAT)_1$$'), 3)
	$(UP)
else
	$(info $(addprefix $(DIR)_, $(addsuffix _1, $(SERVICES))) already up)
endif

down:
ifneq ($(shell docker ps | grep -cE '$(DIR)_$(SERVICES_PAT)_1$$'), 0)
	$(DOWN)
else
	$(info $(addprefix $(DIR)_, $(addsuffix _1, $(SERVICES))) already down)
endif

execp:
ifneq ($(shell docker ps | grep -w $(DIR)_pihole_1),)
	$(EXEC) $(DIR)_pihole_1 bash
else
	$(info $(DIR)_pihole_1 is not running)
endif

execu:
ifneq ($(shell docker ps | grep -w $(DIR)_unbound_1),)
	$(EXEC) $(DIR)_unbound_1 bash
else
	$(info $(DIR)_unbound_1 is not running)
endif

execd:
ifneq ($(shell docker ps | grep -w $(DIR)_dhcp-helper_1),)
	$(EXEC) $(DIR)_dhcp-helper_1 bash
else
	$(info $(DIR)_dhcp-helper_1 is not running)
endif

logp:
ifneq ($(shell docker ps -a | grep -w $(DIR)_pihole_1),)
	$(LOGS) $(DIR)_pihole_1
else
	$(info $(DIR)_pihole_1 was not found)
	$(info $(shell docker ps -a | grep -w $(DIR)_pihole_1))
endif

logu:
ifneq ($(shell docker ps -a | grep -w $(DIR)_unbound_1),)
	$(LOGS) $(DIR)_unbound_1
else
	$(info $(DIR)_unbound_1 was not found)
endif

logd:
ifneq ($(shell docker ps -a | grep -w $(DIR)_dhcp-helper_1),)
	$(LOGS) $(DIR)_dhcp-helper_1
else
	$(info $(DIR)_dhcp-helper_1 was not found)
endif

.make:
	mkdir .make

clean:
ifeq ($(shell docker image ls narvin/pihole:$(PIHOLE_TAG) | wc -l), 2)
	$(RMI) narvin/pihole:$(PIHOLE_TAG)
endif
ifeq ($(shell docker image ls narvin/unbound:$(UNBOUND_TAG) | wc -l), 2)
	$(RMI) narvin/unbound:$(UNBOUND_TAG)
endif
ifeq ($(shell docker image ls narvin/dhcp-helper:$(DHCP_HELPER_TAG) | wc -l), 2)
	$(RMI) narvin/dhcp-helper:$(DHCP_HELPER_TAG)
endif
	$(RM) .make/*

